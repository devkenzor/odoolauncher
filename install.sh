#!/bin/bash

if (( $EUID != 0 )); then
    echo "As the install script need an access to /usr/bin/, please run it with sudo"
    exit
else
    cp odoo /usr/bin/
    cp odoox /usr/bin/
    cp terminator-split /usr/bin/
fi
